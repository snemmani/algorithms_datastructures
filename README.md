# Algorithms and Datastructures

## Author: Shankar Nag Nemmani

## Purpose: Learn and implement various Datastructures and algorithms

### 2019-08-10 
Added [Selection Sort](https://bitbucket.org/snemmani/algorithms_datastructures/src/master/sorting/python/selection-sort.py "Python Implementation")
Added [Selection Sort (Java Implementation)](https://bitbucket.org/snemmani/algorithms_datastructures/src/master/sorting/java/SelectionSort.java "Java Implementation")
