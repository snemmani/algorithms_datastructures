import java.util.ArrayList;

public class SelectionSort {
    public SelectionSort() {

    }

    public void performSort(ArrayList<Integer> array) {
        for (int i=0; i < array.size(); i++ ) {
            Boolean foundMinimum = false;
            int minimum = i;
            for (int j=i; j < array.size(); j++) {
                if ( array.get(j) < array.get(minimum) ) {
                    minimum = j;
                    foundMinimum = true;
                }
            }

            if ( foundMinimum ) {
                int temp = array.get(i);
                array.set(i, array.get(minimum));
                array.set(minimum, temp);
            }
        }
    }

    public static void main(String args[]) {
        ArrayList<Integer> array = new ArrayList<Integer>(){{
            add(32); add(43); add(34); add(45); add(56); add(234); add(1);
        }};
        SelectionSort selectionSort = new SelectionSort();
        System.out.println(array);
        selectionSort.performSort(array);
        System.out.println(array);

    }
}