#!/usr/bin/env python3
import random, unittest


class SelectionSortTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_bubble_sort(self):
        for i in range(100):
            array = [ random.randint(1, 100) for elem in range(30) ]
            array_copy = array.copy()
            selection_sort(array)
            self.assertEqual(sorted(array_copy), array)

def selection_sort(array):
    for i in range(len(array)):
        minimum_index = i
        found_another_min = False
        for j in range(i, len(array)):
            if array[j] < array[minimum_index]:
                minimum_index = j
                found_another_min = True
        if found_another_min:
            array[minimum_index], array[i] = array[i], array[minimum_index]

if __name__=="__main__":
    unittest.main()